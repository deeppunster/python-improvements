# Python Improvements or

# This is not your teacher's Python.

##Purpose

This repository is about improvements made to Python 3 with version 3.6 and
3.7. 
 
A comparison of os.walk vs. os.scandir is alluded to by this presentation.  
The details can be found at https://gitlab.com/deeppunster/Walk_vs_Scan.

## Environment

Showing off 3.7 features requires Python 3.7 of course.  The rise library 
was added to make a "slide show" like experience for the audience.  
Apparently, Jupyter needs a lot of supporting libraries - as noted in the 
requirements.txt file.

## History

The Jupyter notebook contained was presented to the Central Ohio 
Python User Group (COhPy - http://www.cohpy.org/) on April 29, 2019.
